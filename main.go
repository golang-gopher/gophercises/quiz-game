package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

type Problem struct {
	Question string
	Answer   string
}

func parseLines(data [][]string) []Problem {
	problems := make([]Problem, len(data))

	for i, problem := range data {
		problems[i] = Problem{
			Question: problem[0],
			Answer:   strings.TrimSpace(problem[1]),
		}
	}

	return problems
}

func main() {

	csvFilename := flag.String("csv", "problem.csv", "a csv file in the format of 'question, answer'")
	timeLimit := flag.Int("limit", 4, "the time limit for the quiz in seconds")

	flag.Parse()
	// Open file
	csvFile, err := os.Open(*csvFilename)

	if err != nil {
		log.Fatalf("Failed to open CSV file: %v", err)
	}

	defer csvFile.Close()

	csvReader := csv.NewReader(csvFile)
	data, err := csvReader.ReadAll()

	if err != nil {
		log.Fatalf("Failed to read CSV file: %v", err)
	}

	problems := parseLines(data)
	correctAnswer := 0
	timer := time.NewTimer(time.Duration(*timeLimit) * time.Second)

	for index, problem := range problems {
		fmt.Printf("Problem #%d: %s = ", index+1, problem.Question)
		answerChannel := make(chan string)
		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerChannel <- answer
		}()
		select {
		case <-timer.C:
			fmt.Printf("\nYou score %d out of %d\n", correctAnswer, len(problems))
			return
		case answer := <-answerChannel:
			if answer == problem.Answer {
				correctAnswer++
			}
		}

	}

	fmt.Printf("\nYou score %d out of %d\n", correctAnswer, len(problems))
}
